import React from 'react';
import {shallow} from 'enzyme';
import {Counter} from '../Counter';

describe('Counter Component', () => {
    it('renders without crashing #1', () => {
        const counter = shallow(<Counter/>);
        expect(counter.find('div').length).toEqual(2);
    })
});