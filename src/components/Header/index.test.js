import React from 'react';
import {shallow} from 'enzyme';
import Header from '../Header';

describe('Header Component', () => {
    it('renders without crashing #1', () => {
        const header = shallow(<Header/>);
        expect(header.find('div').length).toEqual(1);
    })
    it('renders without crashing #2', () => {
        const header = shallow(<Header/>);
        expect(header.find('div.header').length).toEqual(1);
    })
    it('renders without crashing #3', () => {
        const header = shallow(<Header/>);
        const expectedOutput = '<div class="header"><p class="App-intro">To get started, edit <code>src/App.js</code> and save to reload.</p></div>';
        const realOutput = header.find('div.header').html();
        expect(realOutput.indexOf(expectedOutput)>-1).toEqual(true);
    })
})
